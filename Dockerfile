
FROM ubuntu:22.04 AS build

## install dependencies
RUN apt-get update -y && apt-get install -y \
    wget \
    xz-utils

## download the tor browser
RUN wget https://www.torproject.org/dist/torbrowser/12.5.6/tor-browser-linux64-12.5.6_ALL.tar.xz -O tor-browser.tar.gz

## extract the tor browser
RUN tar -xvf tor-browser.tar.gz


FROM ubuntu:22.04 AS prod

## avoid interactions
ENV DEBIAN_FRONTEND=noninteractive

## install dependencies
RUN apt-get update -y && apt-get install -y \
    zenity \
    kdialog \
    file \
    libdbus-glib-1-2

## create the tor-user account
RUN useradd tor-user -m

## use tor-user for next instructions
USER tor-user

## copy tor browser
COPY --from=build --chown=tor-user:tor-user /tor-browser /tor-browser

WORKDIR /tor-browser

CMD /tor-browser/start-tor-browser.desktop --verbose

