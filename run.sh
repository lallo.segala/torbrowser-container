xhost + local:
docker run \
    -it \
    --rm \
    --name      tor-browser \
    --net       host \
    --env       "DISPLAY" \
    --volume    "$HOME/.Xauthority:/home/tor-user/.Xauthority:rw" \
    --volume    "$HOME/torbrowser-container/tor-data:/tor-browser/Browser/TorBrowser/Data/:rw" \
    --volume    "$HOME/torbrowser-container/tor-downloads:/tor-browser/Browser/Downloads/:rw" \
    --hostname  tor-container \
    registry.gitlab.com/lallo.segala/torbrowser-container:latest $@
